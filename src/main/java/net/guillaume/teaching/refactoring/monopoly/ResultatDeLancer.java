package net.guillaume.teaching.refactoring.monopoly;

public class ResultatDeLancer {

  private final int[] valeursLancer;

  public ResultatDeLancer(int[] valeursLancer) {
    this.valeursLancer = valeursLancer;
  }

  protected boolean estUnDouble() {     // test si c'est un double
    return (valeursLancer[0] == valeursLancer[1]);
  }

  public int faitLaSomme() {    // calcul le total du lancer
    return (valeursLancer[0] + valeursLancer[1]);
  }

}
